package com.jt.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jt.system.pojo.Log;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author hy
 * @since 2021/11/4
 */
@Mapper
public interface LogMapper extends BaseMapper<Log> {
}
