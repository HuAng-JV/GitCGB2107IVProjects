package com.jt.system.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author hy
 * @since 2021/11/1
 */
@Data
@Accessors(chain = true)
@TableName("tb_users")
public class User implements Serializable {
    private static final long serialVersionUID = -9218088594214708448L;
    @TableId(type = IdType.AUTO)
    private Long id;

    private String username;

    private String password;

    private String status;
}
