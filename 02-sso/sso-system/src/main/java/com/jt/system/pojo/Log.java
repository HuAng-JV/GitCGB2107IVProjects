package com.jt.system.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 基于此对象封装用户行为日志
 * 谁在什么时间执行了什么操作，访问了什么方法，
 * 传递了什么参数，访问时长等
 * @author hy
 * @since 2021/11/4
 */
@Data
@Accessors(chain = true)
@TableName("tb_logs")
public class Log implements Serializable {
    private static final long serialVersionUID = 6848310075240307407L;
    @TableId(type = IdType.AUTO)
    private Long id;
    private String username;
    private String operation;
    private String method;
    private String params;
    private Long time;
    private String ip;
    @TableField("createdTime")
    private Date createdTime;
    private Integer status;
    private String error;
}
