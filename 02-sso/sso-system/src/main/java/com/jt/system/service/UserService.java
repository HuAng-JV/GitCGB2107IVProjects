package com.jt.system.service;

import com.jt.system.pojo.User;

import java.util.List;

/**
 * @author hy
 * @since 2021/11/1
 */
public interface UserService {
    User selectUserByUsername(String username);

    List<String> selectUserPermissions(Long userId);
}
