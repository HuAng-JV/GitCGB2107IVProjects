package com.jt.system.service.impl;

import com.jt.system.mapper.LogMapper;
import com.jt.system.pojo.Log;
import com.jt.system.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * @author hy
 * @since 2021/11/4
 */
@Service
public class LogServiceImpl implements LogService {
    @Autowired
    private LogMapper logMapper;

    /**
     * @Async 描述的方法底层会异步执行
     * (不由web服务线程执行，而是交给spring
     * 自带的线程池中的线程去执行，但是 @Async注解的应用有个前提
     * 需要启动类上启动异步执行@EnableAsync)
     * 优点：不会长时间阻塞web服务(例如Tomcat)线程
     * @param log
     */
    @Async
    @Override
    public void insertLog(Log log) {
        logMapper.insert(log);
    }
}
