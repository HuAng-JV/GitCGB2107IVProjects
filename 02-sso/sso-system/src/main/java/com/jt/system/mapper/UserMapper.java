package com.jt.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jt.system.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author hy
 * @since 2021/11/1
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {


    /**
     * 基于用户名查询用户信息
     * @param username
     * @return
     */
    @Select("SELECT id, username, password, " +
            "status " + "from tb_users " +
            "where username = #{username}")
    User selectUserByUsername(String username);

    /**
     * 基于用户id查询用户权限，设计到的表有
     * tb_user_roles(用户角色关系表，可以在此表中基于用户id找到用户角色)
     * tb_role_menus(角色菜单关系表，可以基于角色id找到菜单id)
     * tb_menus(菜单表，菜单为资源的外在表现形式，在此表中可以基于菜单id找到权限标识)
     * 1.三次单表查询
     * 2.嵌套查询
     * 3.多表查询
     * @param userId
     * @return
     */
    @Select("select distinct tm.permission from tb_menus tm " +
            "left join tb_role_menus trm on trm.menu_id = tm.id  " +
            "left join tb_user_roles tur on tur.role_id = trm.role_id " +
            "where tur.id = #{userId}")
    List<String> selectUserPermissions(Long userId);


}
