package com.jt.system;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author hy
 * @since 2021/11/1
 */
@SpringBootTest
public class DataSourceTests {
    //享元模式
    /**
     * 这里的DataSource为Java官方提供一个数据资源接口，所有的连接池都会
     * 基于这个接口进行设计和实现.
     * 这里系统底层会帮我们配置一个HikariCP连接池，因为每次创建链接和销毁链接
     * 占用资源比较多，性能比较差，所以通过连接池实现链接对象的可重用性，所有
     * 池的设计都会中采用享元模式(通过池检查对象的创建次数，实现对象的可重用性)
     */
    @Autowired
    private DataSource dataSource; //HikariDataSource(HikariCP连接池中的对象)
    @Test
    void testGetConnection() throws SQLException {
        Connection connection = dataSource.getConnection();
        System.out.println(connection);
    }
}
