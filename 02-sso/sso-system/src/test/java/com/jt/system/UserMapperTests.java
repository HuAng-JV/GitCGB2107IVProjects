package com.jt.system;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jt.system.mapper.UserMapper;
import com.jt.system.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @author hy
 * @since 2021/11/1
 */
@SpringBootTest
public class UserMapperTests {
    @Autowired
    private UserMapper userMapper;

    @Test
    void testSelectUserByUsername() {
        User user1 = userMapper.selectUserByUsername("admin");
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("username", "admin");
        User user2 = userMapper.selectOne(queryWrapper);
        System.out.println(user2);
    }

    @Test
    void testSelectPermissionsByUserId() {
        List<String> list = userMapper.selectUserPermissions(1L);
        System.out.println(list);
    }

}
