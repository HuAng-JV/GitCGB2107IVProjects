package com.jt.system;

import com.jt.system.mapper.LogMapper;
import com.jt.system.pojo.Log;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;

/**
 * @author hy
 * @since 2021/11/4
 */
@SpringBootTest
public class LogMapperTests {
    @Autowired
    private LogMapper logMapper;

    @Test
    void insertLog() {
        Log log = new Log();
        log.setUsername("hy").setOperation("增").setMethod("Post")
                .setParams("log").setTime(20L).setIp("176.8.2.6")
                .setCreatedTime(new Date()).setStatus(1).setError("0");
        System.out.println(log);
        logMapper.insert(log);

    }
}
