package com.jt.auth.pojo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author hy
 * @since 2021/11/1
 */
@Data
public class User implements Serializable {
    private static final long serialVersionUID = -5151784032253281474L;
    private Long id;
    private String username;
    private String password;
    private String status;
}
