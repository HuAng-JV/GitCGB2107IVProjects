package com.jt.auth.service.impl;



import com.jt.auth.service.RemoteUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 在此对象中实现远程服务调用，从sso-system服务中获取用户信息，
 * 并对用户信息进行封装返回，交给认证管理器(AuthenticationManager)去完成密码的比对操作.
 * request -> filter -> servlet -> Handler Intercept -> controller
 * @author hy
 * @since 2021/11/1
 */
@Service
@Slf4j
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private RemoteUserService remoteUserService;

    /**
     * 执行登录操作时，提交登录按钮系统会调用此方法
     * @param username 客户端用户提交的用户名
     * @return 封装了登录用户信息以及用户权限信息的一个对象，
     * 返回的UserDetails对象最终会交给认证管理器
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        //1、基于用户名查找用户信息，判定用户是否存在
        com.jt.auth.pojo.User user = remoteUserService.selectUserByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("用户不存在!");
        }
        //2、基于用户id查询用户权限（登录用户不一定可以访问所有资源）
        List<String> permissions = remoteUserService.selectUserPermissions(user.getId());
        log.info("permission {}", permissions);
        //3、封装查询结果并返回
        User userInfo = new User(username,
                user.getPassword(),
                AuthorityUtils.createAuthorityList(permissions.toArray(new String[]{})));
        //你怎么知道这里可以new User对象？查UserDetailsService接口实现类
        return userInfo;
    }
}
