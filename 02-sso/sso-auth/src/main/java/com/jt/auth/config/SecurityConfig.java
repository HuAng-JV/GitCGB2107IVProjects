package com.jt.auth.config;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * @author hy
 * @since 2021/11/1
 */
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    /**
     * HttpSession （user info）
     * cookie
     * response 将cookie(cookie中的内容为session对象id)写到客户端
     * browser --> request(cookie) --> server --> httpSession --> user
     * 构建密码加密对象，登录时，系统底层会基于此对象进行加密
     * @return
     */
    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }


    /**
     * 假如在前后端分离架构中，希望对登录成功和失败以后的信息以json
     * 形式进行返回，我们自己控制哪些url需要认证，哪些不需要认证
     * 我们可以重写下面的方法进行自定义
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //1.关闭跨域攻击(这里的登录默认是post请求,
        //但系统底层的跨域攻击设计是不允许post请求的)
        http.csrf().disable();
        //2、放行所有请求url
        http.authorizeRequests()
                .anyRequest().permitAll();
        //3、配置登录成功和失败处理器
        http.formLogin()
                .successHandler(successHandler())
                .failureHandler(failureHandler());
    }

    @Bean
    public AuthenticationSuccessHandler successHandler() {
        return (request, response, authentication) -> {
            HashMap<String, Object> map = new HashMap<>();
            map.put("state", 200);
            map.put("message", "login OK");
            writeJsonToClient(response, map);
        };
    }

    public AuthenticationFailureHandler failureHandler() {
        return (request, response, exception) -> {
            HashMap<String, Object> map = new HashMap<>();
            map.put("state", 500);
            map.put("message", "login error");
            writeJsonToClient(response, map);
        };
    }

    private void writeJsonToClient(HttpServletResponse response,
                                   Map<String, Object> map) throws IOException {
        //将要响应到客户端的数据转换为json格式的字符串
        String json = new ObjectMapper().writeValueAsString(map);
        //设置响应内容的编码
        response.setCharacterEncoding("utf-8");
        //告诉浏览器响应数据的类型以及编码
        response.setContentType("application/json;charset=utf-8");
        //将字符串内容响应到客户端
        PrintWriter out = response.getWriter();
        out.print(json);
        out.flush();
    }

    /**
     * 此对象要为后续的oauth2的配置提供服务
     * 认证管理器对象--用于底层密码比对
     * @return
     * @throws Exception
     */
    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        //super指WebSecurityConfigurerAdapter抽象类，调用了它的authenticationManager()方法
        return super.authenticationManager();
    }

}
