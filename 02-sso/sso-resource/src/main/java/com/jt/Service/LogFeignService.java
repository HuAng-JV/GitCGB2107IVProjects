package com.jt.Service;

import com.jt.resource.pojo.Log;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author hy
 * @since 2021/11/4
 */
@FeignClient(name = "sso-system",contextId = "logFeignService")
public interface LogFeignService {

    @PostMapping("/log/doInsert")
    public void recordLog(@RequestBody Log log);
}
