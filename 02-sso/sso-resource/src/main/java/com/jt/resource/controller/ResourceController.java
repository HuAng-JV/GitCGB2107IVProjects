package com.jt.resource.controller;

import com.jt.resource.annotation.RequiredLog;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * @author hy
 * @since 2021/11/3
 */
@RestController
@RequestMapping("/resource")
public class ResourceController {

    /**
     * 注解描述方法时，表示访问此方法需要具备一定的权限
     * @return
     */
    @PreAuthorize("hasAuthority('sys:res:create')")
    @GetMapping("/select")
    public String getResource() {
        return "get resource by permission";
    }

    @RequiredLog("查询日志") //注解中的内容为一个操作名称
    @PreAuthorize("hasAuthority('sys:res:list')")
    @GetMapping("/doGet")
    public String doGet() {
        return "get resource ok";
    }

    @PutMapping
    public String doPut() {
        return "put resource ok";
    }

    @PostMapping
    public String doPost() {
        return "post resource ok";
    }

    @DeleteMapping
    public String doDelete() {
        return "delete resource ok";
    }

}
