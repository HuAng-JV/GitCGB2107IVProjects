package com.jt.resource;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jt.Service.LogFeignService;
import com.jt.resource.annotation.RequiredLog;
import com.jt.resource.pojo.Log;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Date;

/**
 * @Aspect 注解描述的类型为一个切面类型，在此类中可以定义：
 * 1、切入点(切入扩展逻辑的位置~例如去哪先控制，日志记录，事务处理的位置)
 * @Aspect 描述的类中，通常使用@Pointcut注解进行定义
 * 2、通知方法(在切入点对应的目标方法执行前后要执行逻辑需要写到这样的方法中)
 * @Aspect 描述的类中，通过@Before，@After，@Around，@AfterReturning，@AfterThrowing
 * a: @Before切入点方法执行之前执行
 * b: @After 切入点方法执行之后执行(不管切入点方法是否执行成功了,它都会执行)
 * c: @Around 切入点方法执行之前和之后都可以执行(最重要)
 * d: @AfterReturning 切入点方法成功执行之后执行
 * e: @AfterThrowing 切入点方法执行时出了异常会执行
 * @author hy
 * @since 2021/11/4
 */
@Aspect
@Component
public class LogAspect {
    @Autowired
    private LogFeignService logFeignService;
    /**
     * @Pointcut 注解用于定义切入点，此注解中的内容为切入点表达式
     * @annotation 为注解方式的切入点表达式，此方式的表达式为一种细粒度的切入点表达式，
     * 因为它可以精确到方法，例如我们现在使用RequiredLog注解描述方法时，由它描述的方法
     * 就是一个切入点方法
     */
    @Pointcut("@annotation(com.jt.resource.annotation.RequiredLog)")
    public void doLog() {
        //此方法中不需要写任何内容，只负责承载@Pointcut注解
    }

    /**
     * @Around 注解描述的方法为Aspect中的一个环绕通知方法,在此方法
     * 内部可以控制对目标方法的调用.
     * @param joinPoint 连接点对象,此对象封装了你要执行的切入点方法信息,可以基于
     *           此对象对切入点方法进行反射调用
     * @return 目标执行链中切入点方法的返回值.
     * @throws Throwable
     */
    @Around("doLog()")
    public Object doAround(ProceedingJoinPoint joinPoint) throws Throwable {
        //1.获取目标对象类型(切入点方法所在类的类型)
        Class<?> targetClass = joinPoint.getTarget().getClass();
        //2.获取目标方法
        //2.1获取方法签名(包含方法信息,....)
        MethodSignature signature = (MethodSignature)joinPoint.getSignature();
        //2.2获取方法对象
        Method targetMethod = targetClass.getDeclaredMethod(signature.getName(), signature.getParameterTypes());
        //3.获取方法上的RequiredLog注解内容
        //3.1获取目标方法上注解
        RequiredLog requiredLog = targetMethod.getAnnotation(RequiredLog.class);
        //3.2获取注解中的内容(这个内容为我们定义的操作名)
        String operation = requiredLog.value();
        //4.定义操作状态及结果
        int status = 1;
        String error = "";
        Object result = null;
        //5.获取切入点方法执行之前的信息
        long t1 = System.currentTimeMillis();
        try {
            //手动调用目标执行链(这个执行链中包含切入点方法~目标方法)
            result = joinPoint.proceed();
        } catch (Throwable e) {
            status = 0;
            error = e.getMessage();
            throw e;
        }
        //6.获取切入点方法执行之后的信息
        long t2 = System.currentTimeMillis();
        //7.将用户行为日志,封装到Log对象
        //后续在这里可以通过feign将获取的日志传给system工程,进行日志的记录
        Log logInfo = new Log();
        logInfo.setIp("176.8.2.6"); //后续获取
        //获取登录用户信息(参考了Security官方的代码)
        String username = (String)SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();//用户身份
        logInfo.setUsername(username);
        logInfo.setOperation(operation);
        logInfo.setMethod(targetClass.getName() + "." + targetMethod.getName());
        //new ObjectMapper().writeValueAsString(String) 将字符串转成json
        logInfo.setParams(new ObjectMapper().writeValueAsString(joinPoint.getArgs()));
        logInfo.setTime(t2-t1);
        logInfo.setStatus(status);
        logInfo.setError(error);
        logInfo.setCreatedTime(new Date());
        System.out.println("logInfo=" + logInfo);
        //作业:将Log对象通过Feign或者RestTemplate发送到sso-system工程进行日志记录
        logFeignService.recordLog(logInfo);
        return result;
    }
}
