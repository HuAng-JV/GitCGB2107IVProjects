package com.jt.resource.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;


/**
 * 构建令牌配置对象，在微服务架构中，登录成功后，可以将用户信息进行存储，常用存储方式
 * 1、产生一个随机字符串token，然后基于此字符串将用户信息存储到关系型数据库mysql
 * 2、产生一个随机字符串token，然后基于此字符串将用户信息存储到内存数据库redis
 * 3、基于jwt创建令牌token，在此令牌中存储我们的用户信息，这个令牌不需要写到数据库
 * 在客户端存储即可.
 * 基于如上设计方案，oauth2协议中给出了具体的API实现对象，eg：
 * JdbcTokenStore（用的比较少）
 * RedisTokenStore（中型应用）
 * JwtTokenStore（对性能要求比较高的分布式架构）
 * @author hy
 * @since 2021/11/2
 */
@Configuration
public class TokenConfig {
    /**
     * 定义令牌存储方案 本次选择基于jwt令牌方式存储用户状态
     * @return
     */
    @Bean
    public TokenStore tokenStore() {
        return new JwtTokenStore(jwtAccessTokenConverter());
    }

    /**
     * 配置jwt令牌创建和解析对象
     * @return
     */
    @Bean
    public JwtAccessTokenConverter jwtAccessTokenConverter() {
        JwtAccessTokenConverter converter =
                new JwtAccessTokenConverter();
        converter.setSigningKey(SIGNING_KEY);
        return converter;
    }
    //这里的签名key将来可以写到配置中心
    private static final String SIGNING_KEY = "auth";


}
