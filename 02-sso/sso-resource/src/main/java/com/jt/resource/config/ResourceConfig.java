package com.jt.resource.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

/**
 * @author hy
 * @since 2021/11/3
 */
@Configuration
//启动资源服务默认配置
@EnableResourceServer
//访问资源服务器中的相关方法(@PreAuthorize注解描述的方法)时启动权限检查
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class ResourceConfig extends ResourceServerConfigurerAdapter {

    @Override
    public void configure(HttpSecurity http) throws Exception {
//        super.configure(http);
        //1、关闭跨域攻击
        http.csrf().disable();
        //2、配置资源的访问方式
        http.authorizeRequests().antMatchers("/resource/**")
                .authenticated()
                .anyRequest().permitAll();
        //3、配置资源的访问方式
        //4、配置资源的访问方式

    }

}
