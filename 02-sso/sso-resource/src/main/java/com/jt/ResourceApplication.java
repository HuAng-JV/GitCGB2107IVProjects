package com.jt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * 如何设计对这个资源工程？主要是与业务数据相关的一个工程
 * 如何设计对这个资源工程访问？
 * 1、有些资源可以直接访问，无需认证
 * 2、有些资源必须认证后才可以访问
 * 3、有些资源认证以后，还必须有权限才可以访问
 * @author hy
 * @since 2021/11/3
 */
@SpringBootApplication
@EnableFeignClients
public class ResourceApplication {
    public static void main(String[] args) {
        SpringApplication.run(ResourceApplication.class, args);
    }
}
