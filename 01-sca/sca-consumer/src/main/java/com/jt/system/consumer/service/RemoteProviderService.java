package com.jt.system.consumer.service;

import com.jt.system.consumer.config.ProviderFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author hy
 * @since 2021/10/22
 */

/**
 * @FeignClient 注解描述的接口，用于定义远程调用规范
 * 其中，name属性的值为远端服务名，同时也会将这个名字作为
 * RemoteProviderService接口实现类的Bean对象名字
 */
@FeignClient(name = "sca-provider",contextId = "remoteProviderService",
                fallbackFactory = ProviderFallbackFactory.class)
public interface RemoteProviderService {

    /**
     * 基于此方法进行远程服务调用
     * @param msg
     * @return
     * FAQ：
     * 1）方法上为什么要写@GetMapping这样的注解呢？
     * Feign接口是基于方法上的@GetMapping等注解中的value属性值
     */
    @GetMapping("/provider/echo/{msg}")
    String echoMsg(@PathVariable("msg") String msg);

}
