package com.jt.system;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * @author hy
 * @since 2021/10/21
 */
//当使用这个注解描述配置类时，主要用于告诉spring框架，
// 要对使用@FeignClient注解描述的接口创建其实现类及对象
@EnableFeignClients
@SpringBootApplication
public class ConsumerApplication {
    public static void main(String[] args) {
        SpringApplication.run(ConsumerApplication.class, args);
    }

    /**
     * 构建RestTemplate对象，基于此对象实现远程服务调用
     * 因为spring容器初始化时，没有创建这个对象，所以我们
     * 自己创建，然后交给spring管理
     * @return
     * 在spring中配置第三方的Bean时，可以通过@Bean的方式
     * 对指定方法进行描述，然后在方法内部进行对象的创建和配置。
     * @Bean描述的方法，其返回值会交给spring管理，spring会给
     * 这个bean一个默认的名字，默认为方法名，这个名字对应的bean
     * 的作用域为单例作用域
     */


    @Bean
    //@ConditionalOnMissingBean //spring容器没有这个bean时它才会生效
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    /**
     * 使用@LoadBalanced注解描述RestTemplate对象时，
     * 假如使用RestTemplate对象发起远程服务调用，底层会对这个
     * 请求进行拦截，拦截到此请求后，会基于LoadBalancedClient对象
     * 获取服务实例，然后进行负载均衡方式的调用
     * @return
     */
    @Bean
    @LoadBalanced
    public RestTemplate loadBalancedRestTemplate() {
        return new RestTemplate();
    }
}
