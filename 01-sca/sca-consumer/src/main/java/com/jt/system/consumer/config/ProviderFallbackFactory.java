package com.jt.system.consumer.config;

import com.jt.system.consumer.service.RemoteProviderService;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * 远程服务回调工厂，当远端服务不可用时，可以
 * 通过此工厂创建一个Service
 * @author hy
 * @since 2021/10/25
 */
@Component
public class ProviderFallbackFactory implements FallbackFactory<RemoteProviderService> {
    //FallbackFactory<T> T泛型 表示需要调的远程服务接口
    @Override
    public RemoteProviderService create(Throwable throwable) {
//        return new RemoteProviderService() {
//            @Override
//            public String echoMsg(String msg) {
//                return "服务繁忙，请稍后访问！";
//            }
//        };
        //JDK8中lambda表达式
        return (msg) -> {return "服务繁忙，请稍后访问！";};
    }
}
