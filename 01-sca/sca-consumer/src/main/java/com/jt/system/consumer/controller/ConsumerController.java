package com.jt.system.consumer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * 定义服务消费端Controller，在这个Controller对象
 * 的方法中实现对远端服务sca-provide的调用
 * @author hy
 * @since 2021/10/21
 */
@RestController
public class ConsumerController {
    @Autowired
    private LoadBalancerClient loadBalancerClient;

    @Value("${spring.application.name}")
    private String appName;
    /**
     * 从spring容器获取一个RestTemplate对象，
     *基于此对象实现远端服务调用
     */
    @Autowired
    private RestTemplate restTemplate;
    /**
     * 在此方法中通过一个对象调用远端sca-provider中的服务
     * @return
     * 访问此方法的url：http://localhost:8090/consumer/doRestEcho1
     */
    @GetMapping("/consumer/doRestEcho1")
    public String doRestEcho1() {
        //1、定义要调用的远端服务的url
        String url = "http://localhost:8081/provider/echo/8090";
        //2、基于RestTemplate对象中的相关方法进行服务调用
        return restTemplate.getForObject(url, String.class);
    }


    @GetMapping("/consumer/doRestEcho2")
    public String doRestEcho2() {
        //1、获取服务实例
        ServiceInstance serviceInstance =
                //sca-provider 为nacos服务列表中的服务名
                loadBalancerClient.choose("sca-provider");
        //1、定义要调用的远端服务的url
        String url = String.format("http://%s:%s/provider/echo/%s", serviceInstance.getHost(),serviceInstance.getPort(),appName);
        //2、基于restTemplate调用
        return restTemplate.getForObject(url, String.class);
    }



    /**
     * 在doRestEcho3方法中对doRestEcho2代码实现简化操作
     * 简化：基于loadBalancerClient对象获取服务实例信息的过程
     * 如何简化的？ 为RestTemplate对象注入拦截器，在底层拦截器中
     * 实现服务实例的获取
     * 如何为RestTemplate对象注入拦截器？RestTemplate对象构建时，
     * 使用@LoadBalanced注解描述
     */
    @Autowired
    private RestTemplate loadBalancedRestTemplate;

    @GetMapping("/consumer/doRestEcho3")
    public String doRestEcho3() {
        String serviceId = "sca-provider"; //nacos服务列表中的一个服务名
        String url = String.format("http://%s/provider/echo/%s",serviceId,appName);
        return loadBalancedRestTemplate.getForObject(url, String.class);
    }
}
