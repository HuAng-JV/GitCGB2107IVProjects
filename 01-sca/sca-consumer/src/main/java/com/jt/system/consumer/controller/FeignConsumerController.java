package com.jt.system.consumer.controller;

import com.jt.system.consumer.service.RemoteProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author hy
 * @since 2021/10/22
 */
@RestController
@RequestMapping("/consumer")
public class FeignConsumerController {
    @Autowired
    private RemoteProviderService remoteProviderService;

    /**
     * 在此方法中基于Feign实现远程服务调用
     * @param msg
     * @return
     * 外界访问http://localhost:8090/consumer/echo/aaa
     */
    @GetMapping("/echo/{msg}")
    public String doFeignEcho(@PathVariable("msg") String msg) throws InterruptedException {
        Thread.sleep(5000);
        return remoteProviderService.echoMsg(msg);
    }


}
