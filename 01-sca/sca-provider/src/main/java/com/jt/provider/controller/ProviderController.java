package com.jt.provider.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * 创建一个服务提供方对象，通过此对象
 * 处理服务消费端请求
 * @author hy
 * @since 2021/10/21
 */
@RestController
public class ProviderController {
    @Value("${spring.application.name}")
    private String server;
    /**
     * 请求处理对象通过方法处理客户端或者消费端请求，
     * 当前方法主要用于实现一个字符串回显，就是向客户端
     * 或服务的消费端返回一个字符串消息
     * @param msg
     * @return
     */
    @GetMapping("/provider/echo/{msg}")
    public String doRestEcho1(@PathVariable("msg") String msg) {
        return server + "say hello!" + msg;
    }
}
