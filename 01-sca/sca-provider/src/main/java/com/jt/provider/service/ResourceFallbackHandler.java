package com.jt.provider.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author hy
 * @since 2021/10/28
 */
@Slf4j
@Component
public class ResourceFallbackHandler {
    /**
     * 注意此方法中的异常类型必须为Throwable
     * @param ex
     * @return
     */
    public static String call(Throwable ex){
        log.error("service exception {}", ex.getMessage());
        return "业务处问题了,稍等片刻再访问";
    }
}
