package com.jt.provider.service;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import org.springframework.stereotype.Service;

/**
 * @author hy
 * @since 2021/10/27
 */
@Service
public class ResourceService {
    /**
     *@SentinelResource使用此注解描述的方法
     * 在此方法被访问时，会在sentinel的簇点链路中显示，
     * 次注解中指定的名字就是资源名，我们可以对这个资源
     * 的访问，按照指定的链路进行限流设计
     * @return
     * 此注解中blockHandlerClass用于指定出现限流异常时的异常处理类
     * blockHandler属性用于指定异常处理类中的方法（此方法的返回类型，参数
     * 要与@SentinelResource注解描述的方法一致，参数类型可以加异常类型，
     * 而且方法必须是静态）
     * fallbackClass 用于指定业务异常处理类，fallback用于指向业务处理类
     * 中的异常处理方法（此方法的返回类型，参数要与@SentinelResource注解的）
     */
    @SentinelResource(value = "doGetResource",
                        blockHandlerClass = ResourceBlockHandler.class,
                        blockHandler = "call" )
    public String doGetResource() {
        return "do get resource";
    }

    @SentinelResource(value = "resource")
    public String doFindById(Integer id) {
        return "resource id is " + id;
    }
}
