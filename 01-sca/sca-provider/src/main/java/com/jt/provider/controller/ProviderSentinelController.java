package com.jt.provider.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.jt.provider.service.ResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicLong;

/**
 * @author hy
 * @since 2021/10/27
 */
@RestController
@RequestMapping("/provider")
public class ProviderSentinelController {
    /**
     * 限流：已完成
     * 拓展：通过一个设计限制此方法的访问时间？
     * 你的方案是什么？基于springMVC中的拦截器
     * @return
     */
    @GetMapping("/doSentinel01")
    public String doSentinel01() {
        return "sentinel 01 test";
    }

    @GetMapping("/doSentinel02")
    public String doSentinel02() {
        return "sentinel 02 test";
    }
    @Autowired
    private ResourceService resourceService;

    @GetMapping("/doSentinel03")
    public String doSentinel03() {
        return "sentinel 02 test" + resourceService.doGetResource();
    }

    //构建一个AtomicLong对象，提供了线程安全的自增，自减的操作
    private AtomicLong atomicLong = new AtomicLong(1);
    @GetMapping("/doSentinel04")
    public  String doSentinel04() throws InterruptedException {
        long num = atomicLong.getAndIncrement();
        if (num%2==0) { //模拟慢调用
            Thread.sleep(200);
//            throw new RuntimeException("time out");
        }
        return "sentinel 04 test";
    }

    @GetMapping("/sentinel/findById")
    public String doFindById(@RequestParam("id") Integer id) {
        return resourceService.doFindById(id);
    }
}
