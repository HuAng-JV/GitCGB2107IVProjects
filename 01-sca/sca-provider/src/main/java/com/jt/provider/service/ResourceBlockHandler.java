package com.jt.provider.service;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 注意此方法中的异常类型必须为blockException(它是所有限流，降级)
 * @author hy
 * @since 2021/10/28
 */

@Component
@Slf4j
public class ResourceBlockHandler {
    /**
     * 注意此方法中的异常类型必须为BlockException (
     * 它是所有限流,降级等异常的父类类型),方法的返回
     * 值类型为@SentinelResource注解描述的返回值类型,
     * 方法的其他参数为@SentinelResource注解描述的方法参数,
     * 并且此方法必须为静态方法
     * @param ex
     * @return
     */
    public static String call(BlockException ex){
        log.error("block exception {}", ex.getMessage());
        return "访问太频繁了,稍等片刻再访问";
    }
}
