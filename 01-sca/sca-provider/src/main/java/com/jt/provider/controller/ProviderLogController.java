package com.jt.provider.controller;

import com.alibaba.fastjson.JSON;
import com.jt.common.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * 基于此controller演示配置中心的作用
 * 在这个controller中我们会基于日志对象
 * 进行日志输出测试
 * @author hy
 * @since 2021/10/25
 */
//@Slf4j
@RefreshScope     //告诉系统底层，配置中心内容发生变化，重新构建此对象
@RestController
public class ProviderLogController {
    //创建一个日志对象
    //org.slf4j.Logger (java中的日志API规范,基于这个规范有Log4j,Logback等)
    //org.slf4j.LoggerFactory
    //假如在log对象所在的类上使用了@Slf4j注解，log不在需要我们手动创建
    //lombok会帮我们创建
    private static final Logger log =
            LoggerFactory.getLogger(ProviderLogController.class);

    @GetMapping("/provider/log/doLog01")
    public String doLog01() { // trace<debug<info<warn<error
        log.trace("trace");
        log.debug("debug");
        log.info("info");
        log.warn("warn");
        log.error("error");
        return "log config test";
    }

    /**
     * 请问这个配置何时读取？logLevel属性初始化时
     * 请问logLevel属性何时初始化呢？对象构建时
     * 假如希望logLevel属性值，与配置中心这个配置的值始终是同步的怎么办？
     * 只要修改配置中心的内容，就重新构建对象，然后属性会重新初始化
     */
    @Value("${logging.level.com.jt:debug}")
    private String logLevel;

    @GetMapping("/provider/log/doLog02")
    public String doLog02() {
        log.info("log level is {}", logLevel);
        return "log level is " + logLevel;
    }
}
