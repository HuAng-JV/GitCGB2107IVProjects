package com.jt.provider.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hy
 * @since 2021/10/26
 */
@RestController
@RefreshScope
public class ProviderCacheController {
    @Value("${useLocalCache}")
    private boolean useLocalCache;

    @GetMapping("/provider/cache")
    public String doUseLocalCache() {
        return "useLocalCache'value is" + useLocalCache;
    }

}
