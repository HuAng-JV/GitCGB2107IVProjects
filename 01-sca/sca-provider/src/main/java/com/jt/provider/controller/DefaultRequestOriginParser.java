package com.jt.provider.controller;

import com.alibaba.csp.sentinel.adapter.spring.webmvc.callback.RequestOriginParser;

import javax.servlet.http.HttpServletRequest;

/**
 * 构建RequestOriginParser对象，
 * @author hy
 * @since 2021/10/28
 */
public class DefaultRequestOriginParser implements RequestOriginParser {
    /**
     * 当设置了授权规则后，系统底层拦截到请求，会调用此方法，对请求数据进行解析
     * @param httpServletRequest
     * @return
     * http://ip:port/path?origin=aaa
     */
    @Override
    public String parseOrigin(HttpServletRequest httpServletRequest) {
        //String origin = httpServletRequest.getParameter("origin");
        //return origin;

        //假如基于ip地址，进行黑白名单设计，就可以采用如下方式获取ip地址
        //windows命令行获取ip地址的方式：ipconfig
        String ip = httpServletRequest.getRemoteAddr();
        System.out.println("ip=" + ip);
        return ip;
    }
}
