package com.jt.common;

import com.jt.common.util.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * String 单元测试类
 * @author hy
 * @since 2021/10/21
 */
@SpringBootTest //注解描述的类为Spring工程的单元测试类
public class StringTests {
    /**
     * 在当期测试类方法中，使用了sca-common项目中的StringUtils工具类
     */
    @Test //org.junit.jupiter.api.Test 可以不用public修饰
    void testStringEmpty() {
        String content = null;
        boolean flag = StringUtils.isEmpty(content);
        System.out.println(flag);
    }
}
