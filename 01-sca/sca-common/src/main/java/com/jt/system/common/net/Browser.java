package com.jt.system.common.net;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

/**
 * 模拟一个简易浏览器（Client）
 * @author hy
 * @since 2021/10/25
 */
public class Browser {
    public static void main(String[] args) throws IOException {
        //1、创建网络编程中的客户端对象（Socket）
        //构建是从可以对象时要执行连接计算机（ip），访问计算机中的那个应用的port
        Socket socket = new Socket("127.0.0.1", 9999);
        //2、创建一个输入流对象，读取服务端写到客户端的数据
        InputStream inputStream = socket.getInputStream();
        byte[] buf = new byte[1024];
        int len = inputStream.read(buf);
        String content = new String(buf, 0, len);
        System.out.println(content);
        //3、释放资源
        socket.close();
    }
}
