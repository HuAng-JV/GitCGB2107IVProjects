package com.jt.system.common.thread;



import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 简易服务注册中心测试（基于此，思考Nacos中服务的注册和发现）
 * @author hy
 * @since 2021/10/27
 */
public class RegistryTests {
    /**
     * 存储服务信息的容器(可以简单理解为nacos中存储服务信息的对象)
     */
    private static Map<String, String> registryMap = new HashMap<>();
    //private static Map<String, String> registryMap = new ConcurrentHashMap<>(); //线程安全的线程
    //服务的注册，将服务信息存储到map
    public static void doRegist(String serviceId, String host) {
        registryMap.put(serviceId, host);
    }
    //服务的发现,从map中基于key获取服务实例
    public static String doLookup(String serviceId) {
        return registryMap.get(serviceId);
    }
    public static void main(String[] args) {
        //构建服务名，服务地址，服务名与服务地址一一对应
        String[] serviceIds = {"Service-A","Service-B","Service-C","Service-D"};
        String[] hosts = {"192.168.1.1:8081","192.168.1.2:8082","192.168.1.3:8083","192.168.1.4:8084"};

        Thread t1 = new Thread(){
            @Override
            public void run() {
                for (int i=0;i<serviceIds.length;i++) {
                    doRegist(serviceIds[i], hosts[i]);
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        Thread t2 = new Thread(){
            @Override
            public void run() {
               for (int i =0;i<serviceIds.length;i++) {
                   System.out.println(serviceIds[i]+":"+doLookup(serviceIds[i]));
               }
            }
        };

        t1.start();
        t2.start();

    }
}
