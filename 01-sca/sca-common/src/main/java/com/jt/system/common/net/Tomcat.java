package com.jt.system.common.net;

import java.io.IOException;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * 模拟一个简易的Tomcat
 * 基于java中的网络编程实现（java.net）
 * 网络服务端（ServerSocket）
 * 网络客户端（Socket）
 * @author hy
 * @since 2021/10/25
 */
public class Tomcat {
    public static void main(String[] args) throws IOException {
        //1、创建服务（例如：启动nacos，启动。。。）,并在9999端口进行监听
        //网络中计算机的唯一标识是什么？ip
        //计算机应用程序的唯一标识是什么？port
        ServerSocket server = new ServerSocket(9999);
        System.out.println("server start ...");
        //2、启动服务器监听
        while (true) {
            //监听客户端的连接（这里的socket代表客户端对象）
            Socket socket = server.accept(); //阻塞方法
            //在这里可以将socket对象的信息记录一下.(服务注册)
            //创建输出流对象，向客户端输出hello client
            OutputStream outputStream = socket.getOutputStream();
            byte[] buf = ("HTTP/1.1 200 ok \r\n" +
                    "Content-Type:text/html;charset=utf-8 \r\n" + "\r\n" +
                    "<h2>hello Client</h2>").getBytes();
            outputStream.write(buf);
            outputStream.flush();
        }
    }
}
