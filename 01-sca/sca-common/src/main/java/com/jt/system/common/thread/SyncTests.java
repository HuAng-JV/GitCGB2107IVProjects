package com.jt.system.common.thread;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author hy
 * @since 2021/10/26
 */
public class SyncTests {
    //CopyOnWriteArrayList是一个线程安全的List集合，允许多个线程执行并发更新，但是只能有一个更新成功
    private static List<String> cache = new CopyOnWriteArrayList<>(); //CAS(比较和交换)
    public static List<String> selectAll() {
        if (cache.isEmpty()) {
            System.out.println("get data from database");
            List<String> data = Arrays.asList("a", "b", "c");
            cache.addAll(data);
        }
        return cache;
    }

    public static void main(String[] args) {
        System.out.println(Thread.currentThread().getName());
        System.out.println(selectAll());
    }
}
