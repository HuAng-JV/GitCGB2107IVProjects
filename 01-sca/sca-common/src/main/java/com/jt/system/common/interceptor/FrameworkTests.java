package com.jt.system.common.interceptor;

import java.time.LocalTime;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * 如何理解框架?设计好的一个半成品?(类似简历模板)
 * 框架设计时会有一些对象的定义以及这些对象的执行流程,类似一个执行链.
 * 在当前案例中,我们模拟一个框架中执行链的设计.
 */
//拦截器接口
interface HandlerInterceptor{
    default void before(){}//定义在目标handler方法执行之前执行
    default void after(){}//定义在目标handler方法之后执行
}
//处理器接口
interface Handler{
    void processed();//处理业务的方法
}
//定义一个执行链
class ExecutionChain{//我是执行链的设计者
    //一些拦截器
    private List<HandlerInterceptor> interceptors=
            new CopyOnWriteArrayList<>();
    //业务处理器
    private Handler handler;
    public ExecutionChain(List<HandlerInterceptor> interceptors,Handler handler){
        this.handler=handler;
        this.interceptors.addAll(interceptors);
    }
    public void execute(){//负责执行业务的方法(例如处理请求)
        //1.before
        for(int i=0;i<interceptors.size();i++){
            interceptors.get(i).before();
        }
        //2.processed
        handler.processed();
        //3.after
        for(int i=interceptors.size()-1;i>=0;i--){
            interceptors.get(i).after();
        }
    }
}
public class FrameworkTests {//框架应用者
    public static void main(String[] args) {
        //应用执行链
        //1.创建拦截器
        List<HandlerInterceptor> interceptors=new CopyOnWriteArrayList<>();
        HandlerInterceptor interceptor=new HandlerInterceptor() {
            @Override
            public void before() {
                System.out.println("记录考试开始时间:"+ LocalTime.now());
            }
            @Override
            public void after() {
                System.out.println("记录考试结束时间:"+ LocalTime.now());
            }
        };
        interceptors.add(interceptor);
        //2.创建处理
        Handler handler=new Handler() {
            @Override
            public void processed() {
                System.out.println("同学们开始考试");
            }
        };
        //3.创建执行链
        ExecutionChain chain=
                new ExecutionChain(interceptors,handler);
        //4.执行执行链
        chain.execute();
    }
}
