package com.jt.system.common.util;

/**
 * 自己定义一个操作字符串的工具类
 * @author hy
 * @since 2021/10/21
 */

public class StringUtils {
    /**
     * 判断字符串的值是否为空
     * @param str 需要验证的字符串
     * @return true 表示空
     */
   public static boolean isEmpty(String str) {
       return str == null || "".equals(str);
   }
}
