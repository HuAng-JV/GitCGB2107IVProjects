package com.jt.system.common.thread;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author hy
 * @since 2021/10/27
 */
public class ScheduledExecutorServiceTests {
    public static void main(String[] args) {
        //构建一个负责任务调度的线程池对象，翅中最多有三个核心线程
        ScheduledExecutorService ses
                = Executors.newScheduledThreadPool(3);

        //构建任务对象
        Runnable task = new Runnable() {
            @Override
            public void run() {
                String tName = Thread.currentThread().getName();
                System.out.println(tName + "->" + System.currentTimeMillis());
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        //执行任务对象(定时任务调度):1秒以后开始执行，每隔1秒执行1次
        ses.scheduleAtFixedRate(task,
                1, //初始延迟
                1, //每隔1秒启动1次（玉人舞是否执行结束无关）
                TimeUnit.SECONDS);
    }
}
