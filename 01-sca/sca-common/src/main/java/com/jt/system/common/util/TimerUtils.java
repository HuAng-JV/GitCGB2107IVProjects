package com.jt.system.common.util;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ScheduledExecutorService;

/**
 * 单线程定义任务工具类
 * @author hy
 * @since 2021/10/26
 */
public class TimerUtils {

    public static void main(String[] args) {
        //1、构建执行任务的对象
        //Timer对象创建时会创建一个线程，并且为线程分配一个任务队列
        Timer timer = new Timer();
        //2、构建任务对象
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                System.out.println(System.currentTimeMillis());
                //timer.cancel(); //结束任务调度
            }
        };
        //3、定时执行任务
        timer.schedule(timerTask, //要执行的任务
                1000, //1s以后开始执行
                1000); //每个1s执行1次
        //基于Timer类执行定时任务时，最大的缺陷是多个任务不能并发执行

    }
}
